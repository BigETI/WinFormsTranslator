﻿namespace WinFormsTranslator
{
    public interface ITranslatable
    {
        string TranslatableText
        {
            get;
            set;
        }
    }
}
